function compareByValue(x, y)
    a = split(x, "")
    if a[1] == "2" || a[1] == "3" || a[1] == "4" ||  a[1] == "5" ||  a[1] == "6" || a[1] == "7" || a[1] == "8" || a[1] == "9"
        inta1 = parse(Int8, a[1])
    elseif a[1] == "1"
        inta1 = 10
    elseif a[1] == "J"
        inta1 = 11
    elseif a[1] == "Q"
        inta1 = 12
    elseif a[1] == "K"
        inta1 = 13
    elseif a[1] == "A"
        inta1 = 14
    end
    b = split(y, "")
    if b[1] == "2" || b[1] == "3" || b[1] == "4" ||  b[1] == "5" || b[1] == "6" || b[1] == "7" || b[1] == "8" || b[1] == "9"
        intb1 = parse(Int8, b[1])
    elseif b[1] == "1"
        intb1 = 10
    elseif b[1] == "J"
        intb1 = 11
    elseif b[1] == "Q"
        intb1 = 12
    elseif b[1] == "K"
        intb1 = 13
    elseif b[1] == "A"
        intb1 = 14
    end
    if inta1 < intb1
        return true
    else
        return false
    end
end

function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

function insercao(v)
    tam = length(v)
    for i in 2:tam
        j = i
        while j > 1
            if compareByValueAndSuit(v[j], v[j - 1])
                troca(v, j, j - 1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
end

function compareByValueAndSuit(x, y)
    a = split(x, "")
    if a[1] == "1"
        if a[3] == "♦"
            naipea = 1
        elseif a[3] == "♠"
            naipea = 2
        elseif a[3] == "♥"
            naipea = 3
        elseif a[3] == "♣"
            naipea = 4
        end
    else
        if a[2] == "♦"
            naipea = 1
        elseif a[2] == "♠"
            naipea = 2
        elseif a[2] == "♥"
            naipea = 3
        elseif a[2] == "♣"
            naipea = 4
        end
    end
    b = split(y, "")
    if b[1] == "1"
        if b[3] == "♦"
            naipeb = 1
        elseif b[3] == "♠"
            naipeb = 2
        elseif b[3] == "♥"
            naipeb = 3
        elseif b[3] == "♣"
            naipeb = 4
        end
    else
        if b[2] == "♦"
            naipeb = 1
        elseif b[2] == "♠"
            naipeb = 2
        elseif b[2] == "♥"
            naipeb = 3
        elseif b[2] == "♣"
            naipeb = 4
        end
    end
    if naipea < naipeb
        return true
    end
    if naipea == naipeb
        return compareByValue(x, y)
    end
    if naipea > naipeb
        return false
    end
end

using Test

function testacompareByValueAndSuit()
    println("Início dos testes")
    @test compareByValueAndSuit("2♠", "A♠") == true
    @test compareByValueAndSuit("10♦", "2♣") == true
    @test !compareByValueAndSuit("J♣", "A♥") == true
    @test !compareByValueAndSuit("Q♠", "K♦") == true
    @test !compareByValueAndSuit("K♥", "10♥") == true
    @test compareByValueAndSuit("10♠", "10♥") == true
    @test !compareByValueAndSuit("4♣", "2♥") == true
    @test !compareByValueAndSuit("A♦", "J♦") == true
    println("Final dos testes")
end

testacompareByValueAndSuit()
